# import re
# value = '139123456781'
# res = re.match(r'1[3-9]\d{9}$', value)
# print(res)


# from fdfs_client.client import Fdfs_client
from rest_framework.response import Response

# a = 12
# print('a: {}'.format(a))

'''
购物车数据存储
存放的数据： sku_id  counts  selected （商品skuID， 商品数量， 状态是否选中）
1. 登录用户
    存储位置： redis
    Set： {sku_id_1,sku_id_2,...}  存放状态勾选了的商品，未勾选的商品不存放
    Hash：{sku_id_1：count, sku_id_2：count} 存放 商品id：商品的数量
    
2. 未登录用户
    存储位置： cookie
    {
        sku_id_1: {count: 1, selected: True},
        sku_id_2: {count: 2, selected: False},
    }
    
    
3. 购物车数据合并，点击付款时，未登录用户需要先登录或注册在进行付款，登录时需要将cookie和redis中的数据合并
    a：cookie和redis中有相同商品时
        1）商品的数量以cookie中的为准，覆盖redis中的数量
        2）商品勾选状态，cookie和redis中只要有一方勾选则勾选
    b：cookie和redis中没有相同商品时
        1）redis和cookie中的商品直接合并
'''


import pickle, base64
dic = {
    'a': {'name': 'abc', 'address': 'chengdu'},
    'b': {'name': 'xyz', 'address': 'beijing'}
}
# byt = pickle.dumps(dic)
# b64_byt = base64.b64encode(byt)
# str = b64_byt.decode()
# print(type(byt), byt)
# print(type(b64_byt), b64_byt)
# print(type(str), str)

str1 = base64.b64encode(pickle.dumps(dic)).decode()
print(str1)


ss = 'gASVSgAAAAAAAAB9lCiMAWGUfZQojARuYW1llIwDYWJjlIwHYWRkcmVzc5SMB2NoZW5nZHWUdYwBYpR9lChoA4wDeHl6lGgFjAdiZWlqaW5nlHV1Lg=='
# str_byt = ss.encode()
# pik_byt = base64.b64decode(str_byt)
# dic1 = pickle.loads(pik_byt)
#
# print(type(str_byt), str_byt)
# print(type(pik_byt), pik_byt)
# print(type(dic1), dic1)

dict2 = pickle.loads(base64.b64decode(ss.encode()))
# print(dict2)
