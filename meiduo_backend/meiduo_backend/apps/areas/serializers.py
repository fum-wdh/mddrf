from rest_framework import serializers
from .models import Areas

"""
查询所有省时， AreasProvincesSerializer 查出所有的省
查询单一省时， AreasProvincesSerializer代表单个省， AreasDetailSerializer代表省下的所有市
查询单一市时， AreasProvincesSerializer代表单个市， AreasDetailSerializer代表省下的所有区
"""


class AreasProvincesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Areas
        fields = ['id', 'name']


class AreasDetailSerializer(serializers.ModelSerializer):

    # 嵌套序列化
    children = AreasProvincesSerializer(many=True)   # 序列化所有
    # children = serializers.PrimaryKeyRelatedField()  # 只序列化id
    # children = serializers.StringRelatedField()  # 序列化模型中str方法的返回值

    class Meta:
        model = Areas
        fields = ['id', 'name', 'children']