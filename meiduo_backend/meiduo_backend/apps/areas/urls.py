from django.urls import path, re_path
from rest_framework.routers import DefaultRouter
from . import views

urlpatterns = [
    # # 获取所有省份
    # path('provinces/', views.AreasProvincesView.as_view()),
    # # 获取省，市下的所有行政区域
    # re_path(r'^provinces/(?P<area_id>\d+)/', views.AreasDetailsView.as_view())
]

# 视图集自动生成路由
router = DefaultRouter()
router.register(r'provinces', views.AreaViewSet, basename='')
urlpatterns += router.urls
