from django.contrib import admin
from . import models


class AreasAdmin(admin.ModelAdmin):
    list_display = ['id', 'parent', 'name']


# Register your models here.
admin.site.register(models.Areas, AreasAdmin)

from django_crontab import crontab