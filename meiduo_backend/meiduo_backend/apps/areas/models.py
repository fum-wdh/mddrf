from django.db import models


class Areas(models.Model):
    """
    省市区自关联表
    """
    name = models.CharField(max_length=20, verbose_name="行政区域名称")
    parent = models.ForeignKey('self', verbose_name="上级行政区域", on_delete=models.SET_NULL, null=True, blank=True,
                               related_name="children")

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '省市区行政区域'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
