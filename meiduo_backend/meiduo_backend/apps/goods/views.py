from django.shortcuts import render
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView
from .serializers import SKUGoodsSerializer
from .models import SKU


class SKUGoodsView(ListAPIView):
    """获取商品列表数据"""
    serializer_class = SKUGoodsSerializer

    filter_backends = [OrderingFilter]  # 指定过滤后端为排序过滤
    ordering_fields = ['create_time', 'price', 'sales']  # 指定排序字段 (查询所有数据接口 中查的是那个模型中的数据,里面就指定那个模型的字段)

    def get_queryset(self):
        category_id = self.kwargs.get('category_id')
        return SKU.objects.filter(is_launched=True, category_id=category_id)