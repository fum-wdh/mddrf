from rest_framework import serializers
from .models import SKU


class SKUGoodsSerializer(serializers.ModelSerializer):
    """商品列表详情序列化"""
    class Meta:
        model = SKU
        fields = ['id', 'name', 'price', 'default_image_url', 'comments']