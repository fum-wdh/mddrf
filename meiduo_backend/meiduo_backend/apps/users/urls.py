from django.urls import path, re_path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    # 注册用户
    path('register/', views.RegisterUsers.as_view()),
    # 验证用户名是否重复
    re_path(r'^repeated_username/(?P<username>\w{5,15})/count/$', views.UsernameCount.as_view()),
    # 验证手机号是否重复
    re_path(r'^repeated_mobile/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCount.as_view()),
    # jwt登录
    # path('login/', obtain_jwt_token),  # django封装的认证逻辑  登录成功生成token
    path('login/', views.UserAuthorizeView.as_view()),

    # 用户中心详情
    path('details/', views.UserCenterDetails.as_view()),
    # 设置邮箱
    path('email/', views.UserCenterDetailsEmail.as_view()),
    # 邮箱验证
    path('email/verification/', views.EmailVerifyView.as_view()),
    # 商品浏览记录
    path('browse_histories/', views.UserBrowserHistoryView.as_view())

]

router = DefaultRouter()
router.register(r'addresses', views.AddressView, basename='')
urlpatterns += router.urls