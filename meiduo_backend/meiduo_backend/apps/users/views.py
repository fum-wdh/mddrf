from datetime import datetime
from django_redis import get_redis_connection
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView, UpdateAPIView, CreateAPIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import UpdateModelMixin
from rest_framework import status
from rest_framework_jwt.views import ObtainJSONWebToken
from rest_framework_jwt.settings import api_settings

from carts.utils import merge_cart_cookie_to_redis
from .models import User, Address
from .serializers import CreateUserSerializers, UserCenterDetailserializers, UserCenterDetailsEmailSerializers, \
    UserAddressSerializer, AddressTitleSerializer, UserBrowserHistorySaveSerializer, UserBrowserHistoryDisplaySerializer
from goods.models import SKU


# 注册用户
class RegisterUsers(CreateAPIView):
    # 指定序列化器
    serializer_class = CreateUserSerializers


class UsernameCount(APIView):
    # 判断用户是否已注册
    def get(self, request, username):
        username_cont = User.objects.filter(username=username).count()
        data = {
            'username': username,
            'count': username_cont
        }
        return Response(data)


class MobileCount(APIView):
    # 判断手机号是否重复
    def get(self, request, mobile):
        mobile_cont = User.objects.filter(mobile=mobile).count()
        data = {
            'mobile': mobile,
            'count': mobile_cont
        }
        return Response(data)


# GET /details
class UserCenterDetails(RetrieveAPIView):
    """
    get 获取用户详情页
    """
    # 添加权限，登录后才能访问
    permission_classes = [IsAuthenticated]
    serializer_class = UserCenterDetailserializers

    def get_object(self):
        # print(self.request.user.password)
        return self.request.user


# PUT /details/email
class UserCenterDetailsEmail(UpdateAPIView):
    """put 修改用户中心邮箱"""
    permission_classes = [IsAuthenticated]
    serializer_class = UserCenterDetailsEmailSerializers

    def get_object(self):
        return self.request.user


class EmailVerifyView(APIView):
    """通过邮箱激活账号"""

    def get(self, request):
        # 获取token字段
        token = request.query_params.get('token')
        if token is None:
            return Response({'message': '缺少token参数'}, status=status.HTTP_400_BAD_REQUEST)
        # 解密token
        user = User.check_verify_email_token(token)
        if user is None:
            return Response({'message': '链接超时或激活失败'}, status=status.HTTP_400_BAD_REQUEST)
        # 解密成功，则修User模型中 email_active 字段为True
        user.email_active = True
        user.save()
        return Response({'message': '激活成功'})


class AddressView(GenericViewSet, UpdateModelMixin):
    """用户收货地址curd"""
    permission_classes = [IsAuthenticated]
    # 序列化器
    serializer_class = UserAddressSerializer

    # 指定查询集
    def get_queryset(self):
        # 从请求对象中获取用户的所有没有逻辑删除的地址
        return self.request.user.addresses.filter(is_deleted=False)

    # POST /api/users/addresses
    def create(self, request):
        """新增用户地址"""
        user = request.user
        count = Address.objects.filter(user=user).count()
        if count > 20:
            return Response({'message': '收货地址数量达到上线'}, status=status.HTTP_400_BAD_REQUEST)
        # 创建序列化器进行反序列化数据
        serializer = self.get_serializer(data=request.data)
        # 调用序列化器的is_valid方法进行数据校验
        serializer.is_valid(raise_exception=True)
        # 保存数据
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    # GET /api/users/addresses
    def list(self, request):
        """获取用户地址列表"""
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        user = self.request.user
        return Response({
            'addresses': serializer.data,
            'limit': 20,
            'default_address_id': user.default_address_id,
            'user_id': user.id
        })

    # 修改地址继承 UpdateModelMixin 类完成

    # DELETE /api/users/addresses/{address_id}
    def destroy(self, request, *args, **kwargs):
        """逻辑删除地址"""
        address = self.get_object()
        address.is_deleted = True
        address.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    # PUT /api/users/addresses/{address_id}/title
    @action(methods=['PUT'], detail=True)
    def title(self, request, pk=None):
        """修改收货地址title"""
        address = self.get_object()
        serializer = AddressTitleSerializer(instance=address, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    # PUT /api/users/addresses/{address_id}/default_address
    @action(methods=['PUT'], detail=True)
    def default_address(self, request, pk):
        """修改默认收货地址"""
        address = self.get_object()
        request.user.default_address = address
        print('request', request)
        request.user.save()
        return Response({'message': 'success'}, status=status.HTTP_201_CREATED)


class UserBrowserHistoryView(CreateAPIView):
    """用户商品浏览记录"""
    serializer_class = UserBrowserHistorySaveSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """展示浏览记录"""
        redis_conn = get_redis_connection('default')
        user = request.user
        sku_ids = redis_conn.lrange('history_{}'.format(user.id), 0, -1)

        # 通过sku_id获取SKU商品模型对象
        # SKU.objects.filter(id__in=sku_ids)  # 取出来后无序的
        sku_list = []
        for sku_id in sku_ids:
            sku = SKU.objects.get(id=sku_id)
            sku_list.append(sku)

        # 创建序列化器
        serializer = UserBrowserHistoryDisplaySerializer(instance=sku_list, many=True)

        return Response(serializer.data)


jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


class UserAuthorizeView(ObtainJSONWebToken):
    """登录认证(重写jwt自带的登录方法，实现购物车数据登录合并)"""

    # POST /api/users/login/
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            # 登录时调用合并购物车数据方法
            merge_cart_cookie_to_redis(request, user, response)
            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
