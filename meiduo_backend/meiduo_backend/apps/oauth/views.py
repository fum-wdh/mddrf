from QQLoginTool.QQtool import OAuthQQ
from rest_framework.response import Response
from rest_framework.views import APIView


class QQAuthURLView(APIView):
    """qq登录（未完成）"""
    def get(self, request):
        # 获取指定的next值，不存在则获取默认值 /
        next = request.query_params.get('next', '/')
        # QQ登录参数
        QQ_CLIENT_ID = '101514053'
        QQ_CLIENT_SECRET = '1075e75648566262ea35afa688073012'
        QQ_REDIRECT_URI = 'http://www.meiduo.site:8080/oauth_callback.html'

        # 创建qq登录对象
        oauth = OAuthQQ(client_id=QQ_CLIENT_ID, client_secret=QQ_CLIENT_SECRET, redirect_uri=QQ_REDIRECT_URI,
                        state=next)
        # 获取登录跳转url
        login_url = oauth.get_qq_url()

        return Response('login_url', login_url)
