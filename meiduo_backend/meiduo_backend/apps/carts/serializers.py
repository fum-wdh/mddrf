from rest_framework import serializers

from goods.models import SKU


class CartsItemsAddSerializer(serializers.Serializer):
    """新增、修改购物车商品的序列化器"""

    sku_id = serializers.IntegerField(label='商品id', min_value=1)
    count = serializers.IntegerField(label='购买数据')
    selected = serializers.BooleanField(default=True, label='商品勾选状态')

    def validate_sku_id(self, value):
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('没有此商品')
        return value


class CartsItemsGetSerializer(serializers.ModelSerializer):
    """购物车查询序列化器"""
    count = serializers.IntegerField(label='购买数据')
    selected = serializers.BooleanField(label='商品勾选状态')

    class Meta:
        model = SKU
        fields = ['id', 'name', 'price', 'default_image_url', 'count', 'selected']


class CartsItemsDeleteSerializer(serializers.Serializer):
    """删除购物车数据序列化器"""
    sku_id = serializers.IntegerField(label='商品id', min_value=1)

    def validate_sku_id(self, value):
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('没有此商品')
        return value


class CartsSelectedAllSerializer(serializers.Serializer):
    """购物车全选"""
    selected = serializers.BooleanField(label='商品勾选状态')