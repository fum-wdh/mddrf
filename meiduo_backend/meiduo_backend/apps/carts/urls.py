from django.urls import path
from . import views

urlpatterns = [
    path('items/', views.CartsItemsView.as_view()),
    path('items/selection/', views.CartsSelectedAllView.as_view())
]