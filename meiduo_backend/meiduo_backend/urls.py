"""meiduo_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.documentation import include_docs_urls
# 配置swagger各个参数
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Django-DRF API",
        default_version="v1.0.0",
        description="Welcome to the world of Django-DRF",
    ),
    public=True,
)


urlpatterns = [
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    # re_path(r'^docs/', include_docs_urls(title='meiduo API docs')),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),

    path('admin/', admin.site.urls),  # admin/abcd123456
    re_path(r'^api/sms/', include('sms.urls')),
    re_path(r'^api/users/', include('users.urls')),
    # qq登录
    re_path(r'^oauth/', include('oauth.urls')),
    # 省市区
    re_path(r'^areas/', include('areas.urls')),
    # 商品
    re_path(r'^goods/', include('goods.urls')),
    # 购物车
    re_path(r'^carts/', include('carts.urls'))
]
