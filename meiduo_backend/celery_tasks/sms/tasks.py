from celery_tasks.sms.yuntongxun.sms import CCP
from celery_tasks.main import celery_app
from celery_tasks import configurations

# celery -A celery_tasks.main worker -l INFO --pool=eventlet
# celery -A celery_tasks.main worker -l INFO --pool=solo


@celery_app.task(name="send_sms_code")  # 注册celery任务
def send_sms_code(mobile, sms_code):
    """
    :param mobile: 手机号
    :param sms_code: 验证码
    """
    # print(configurations.SMS_CODE_REDIS_EXPIRES)
    # import time
    # time.sleep(5)
    CCP().send_template_sms(mobile, [sms_code, configurations.SMS_CODE_REDIS_EXPIRES // 60], 1)
